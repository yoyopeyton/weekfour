#Peyton Oakes-Bryden
#23 September, 2015
#Coding Quiz 2

#Problem 1
'''
x = 0
y = 0
blueIsWhite = pickAFile()
picture = makePicture(blueIsWhite)
def whiteBlue():
  for pixel in getPixels(picture):
    blueIs = getBlue(pixel)
    if(blueIs < 150):
      setColor(pixel, white)
whiteBlue()
show(picture)
'''

#Problem 2 
'''
doubleBlue = pickAFile()
picture = makePicture(doubleBlue)
def doubleBlu():
  for pixel in getPixels(picture):
    blueX = getBlue(pixel) * 2
    redX = getRed(pixel) / 2
    greenX = getGreen(pixel) / 2
    color = makeColor(redX,greenX,blueX)
    setColor(pixel, color)
   
doubleBlu()
show(picture)
'''

#Problem 3
'''
doubleRed = pickAFile()
picture = makePicture(doubleRed)
def doubleRe():
  for pixel in getPixels(picture):
    blueX = getBlue(pixel) / 2
    redX = getRed(pixel) * 2
    greenX = getGreen(pixel) / 2
    color = makeColor(redX,greenX,blueX)
    setColor(pixel, color)
   
doubleRe()
show(picture)
'''

#Problem 4 NOT WORKING
'''
grayscalePic = pickAFile()
picture = makePicture(grayscalePic)

def grayScaler():
  for pixel in getPixels(picture):
    intensity = (getRed(pixel)+getGreen(pixel)+getBlue(pixel))/3
    setColor(pixel,makeColor(intensity,intensity,intensity))
    
def grayscaleNegative():
  for pixel in getPixels(picture):
    red=getRed(pixel)
    green=getGreen(pixel)
    blue=getBlue(pixel)
    negColor=makeColor(255-red, 255-green, 255-blue)
    setColor(pixel,negColor)

grayScaler()
grayscaleNegative()
show(picture)
'''
#Problem 5 
'''
gsPlus = pickAFile()
picture = makePicture(gsPlus)

def plusColor():
  for pixel in getPixels(picture):
    blueX = getBlue(pixel) + 75
    redX = getRed(pixel) + 75 
    greenX = getGreen(pixel) +75
    color = makeColor(redX,greenX,blueX)
    setColor(pixel, color)

def grayscaler():
  for pixel in getPixels(picture):
    intensity = (getRed(pixel)+getGreen(pixel)+getBlue(pixel))/3
    setColor(pixel,makeColor(intensity,intensity,intensity))

plusColor()
grayscaler()
show(picture)
'''
#Problem 6
'''
topBlackPic = pickAFile()
picture = makePicture(topBlackPic)

def halfBlack():
  x=getWidth(picture)
  y=getHeight(picture) /2 
  addRectFilled(picture,0,0,x,y,black)
  
halfBlack()
show(picture)
'''
#Problem 7 

mPic = pickAFile()
picture = makePicture(mPic)

height=getHeight(picture) / 2
width=getWidth(picture)
fullHeight=getHeight(picture)

for x in range(0,width):
  for y in range(0,height):
    pixel=getPixelAt(picture,x,y)
    upColor=getColor(pixel)
    newHeight=(fullHeight-1)-y
    targetPixel=getPixelAt(picture,x,newHeight)
    complete=setColor(targetPixel,upColor)
    
show(picture)
'''

#Below is dumb experimental mirror image code
'''
#def mirrorHalf():
  pixels=getPixels(picture)
  target=len(pixels)-1
  for index in range(0,len(pixels)/2):
    pixel1=pixels[index]
    color1 = getColor(pixel1)
    pixel2 = pixels[target]
    setColor(pixel2,color1)
    target = target - 1 
    
mirrorHalf()
show(picture)
'''


  